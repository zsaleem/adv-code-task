## Requirements
Below is the stack I used in this project. Please follow the getting started section in order to make this project run.

## Stack
 - HTML5
 - Sass
 - ES6
 - ES6 Promises
 - jQuery
 - Git
 - Gitlab
 - NPM
 - Vim
 - Mac OS X.
 - Google Chrome Incognito
 - Webpack
 - Babel
 - nodejs(7.3.0)
 - Handlebars

## Getting started
In order to run this project in the browser successfully, please follow the steps below!

    1. Clone this repository.
    2. Run npm install command to download and install all Webpack dependencies.
    3. Run `npm run dev` to run the server on port `2770`. This command will run the project in development mode.
    4. To make it ready for deploy run `npm run build`. This command will minify all the resources in `/dist` folder.

## Description
Above steps, in getting started section, will install all dependencies required for this project to run and make the project ready for
production by minifying all resources. It will place the production ready project in `dist` folder at `root` level.

In this task, I followed MV* pattern. I created 2 folders inside `src/scripts/` i.e. `models` and `views`. Inside `models` I wrote 1 model i.e. `DonationModel` which is responsible for getting data from mock json file inside `src/dataset/` folder. In addition, `DonationModel` also updates data.

In `views` folder, I wrote 1 view i.e. `DonationView`. I feed data to this view from its respective `model`(mentioned in previous paragraph). This view requires 1 parameter at the time of initialization(which happens in `src/index.js` file) which is DonationModel instance. The render function inside view is responsible to get data from model and renders on the page. The updateValues function gets updated data from the model and rerenders the data on the screen. ValidateFields function validates input values. resetElements function resets dom elements whereas updateBar function updates the bar.

I wrote all CSS(Sass) in `src/styles/sass` folder. I created 1 file i.e. `Donation.scss` which consists of all styles for this table.

I tested this task on Google Chrome Incognito and MacBook Air with a resolution of 1440x900.

## Notes
Regarding the size of the widget, I kept the size of this widget to 400px. Since I found the contents of the widget are less therefore, making this widget of the size 1000x666 will allow this plenty of empty space.

Regarding responsiveness, as I mentioned above I kept its size to 400px which should be fine for most of the screen sizes.

Moreover, I also added a video demo inside `demo` folder. Please watch this widget in action.

