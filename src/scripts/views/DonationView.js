import $ from 'jquery';
import DonationTemplate from '../../templates/Donation.handlebars';

class DonationView {

    constructor(model) {
        this._model = model;
        this._width = 0;
        this.$body = $('body');

        this._config = {
            btnDonation: '.btnDonation',
            error: 'error',
            donationField: '.donationField',
            container: '.container'
        };

        this._amount = null;

        this.render();

        this.$body.on('click', '.btnDonation', this.getAmount.bind(this));
        this.$body.on('keyup', '.donationField', this.validateField.bind(this));
    }

    render() {
        this._model.get().then((response) => {
            $(this._config.container).html(DonationTemplate(response));

            this.resetElements();
        });
    }

    getAmount() {
        this._amount = $(this._config.donationField).val();
        this.updateValues();
    }

    updateValues() {
        this._model.update(this._amount).then((response) => {
            $(this._config.donationField).removeClass(this._config.error);
            $(this._config.container).html(DonationTemplate(response));

            this.resetElements();

            this.updateBar(response.DonationThreshold, response.DonatedAmount);
        }, (error) => {
            console.log(error);
            $(this._config.donationField).addClass(this._config.error);
        });
    }

    resetElements() {
        $(this._config.btnDonation).prop('disabled', true);
        $(this._config.btnDonation).addClass('disabled');

        $(this._config.donationField).focus();
    }

    updateBar(threshold, donatedAmount) {
        this._width = donatedAmount / threshold * 100;

        if (this._width === 100) {
            $('.fill').addClass('complete');
            $('.donationTooltip').fadeOut(1000);
        }

        $('.fill').animate({
            width: this._width + '%'
        });
    }

    validateField(e) {
        let amount = parseInt($(e.target).val());

        if (!isNaN(amount) && typeof amount === 'number' && amount <= 500) {
            $(this._config.btnDonation).prop('disabled', false);
            $(this._config.btnDonation).removeClass('disabled');
        } else {
            $(this._config.btnDonation).prop('disabled', true);
            $(this._config.btnDonation).addClass('disabled');
        }
    }
};

export default DonationView;

