import $ from 'jquery';

class DonationModel {

    constructor() {
        this._dataset = null;
        this._url = 'http://localhost:2770/src/dataset/donation.json';
    }

    get(amount) {
        return $.ajax({
            url: this._url,
            method: 'GET',
            type: 'JSON'
        }).then((response) => {
            this._dataset = response;
            return this._dataset;
        });
    }

    update(amount) {
        amount = parseInt(amount);

        return new Promise((resolve, reject) => {

            if (amount <= this._dataset.DonationNeeded && amount <= 500) {
                this._dataset.TotalDonors++;

                if (this._dataset.DonationNeeded >= 0 && this._dataset.DonationNeeded <= 500) {
                    this._dataset.DonationNeeded = this._dataset.DonationNeeded - amount;
                }

                if (this._dataset.DonatedAmount <= 500) {
                    this._dataset.DonatedAmount = this._dataset.DonatedAmount + amount;
                }
            }

            resolve(this._dataset);
        });
    }
}

export default DonationModel;

