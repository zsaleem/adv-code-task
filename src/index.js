import DonationModel from './scripts/models/DonationModel';
import DonationView from './scripts/views/DonationView';

let donationModel = new DonationModel();
let donationView = new DonationView(donationModel);

